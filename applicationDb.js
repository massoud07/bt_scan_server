// Massoud Ahmadi
// CS 890 - Topics on Communications
// Final project
// March 29, 2019
//
// 

// Class based on guide from https://stackabuse.com/a-sqlite-tutorial-with-node-js/
const Sqlite3 = require('sqlite3');
const Promise = require('bluebird');

/**
 * Wrapper class for interacting with local sqlite3 database
 */
class ApplicationDB {  

  /**
   * Constructor
   */
  constructor(dbFilePath) {
    this._db = new Sqlite3.Database(dbFilePath, Sqlite3.OPEN_READWRITE | Sqlite3.OPEN_CREATE, (err) => {
      if (err) {
        console.log('Could not connect to database', err);
      } else {
        console.log('Connected to database');
      }
    });
  }

  /**
   * Run incoming sql command and return output
   */
  run(sql, params = []) {
    return new Promise((resolve, reject) => {
      this._db.run(sql, params, function (err) {
        if (err) {
          console.log('Error running sql ' + sql);
          console.log(err);
          reject(err);
        } else {
          resolve({ id: this.lastID });
        }
      });
    });
  }

  /**
   * Make sql queries  
   */
  get(sql, params = []) {
    return new Promise((resolve, reject) => {
      this._db.get(sql, params, (err, result) => {
        if (err) {
          console.log('Error running sql: ' + sql);
          console.log(err);
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  /**
   * Return all items from table
   */
  all(sql, params = []) {
    return new Promise((resolve, reject) => {
      this._db.all(sql, params, (err, rows) => {
        if (err) {
          console.log('Error running sql: ' + sql);
          console.log(err);
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }
}

module.exports = ApplicationDB;