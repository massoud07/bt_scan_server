// Massoud Ahmadi
// CS 890 - Topics on Communications
// Final project
// March 29, 2019
//
//

const admin = require('firebase-admin');
var serviceAccount = require('./btscanner-1552766373091-93ca56e60d4d.json');

/**
 * Wrapper class for google firestore database
 */
class CloudDB {
    /**
     * Constructer will authenticate and initialize firestore database
     */
    constructor(){
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount)
        });
        
        this._db = admin.firestore();
        this._collection = this._db.collection('devices-uofr');
    }

    /**
     * Add or update data to firestore collection
     */    
    set(data, settings){
        return new Promise((resolve, reject) => {        
            this._collection.add(data).catch((err) => {
                console.log('Error saving to firestone db ' + data);
                reject(err);
            });

            resolve();
        });
    }
}

module.exports = CloudDB;