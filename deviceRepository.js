// Massoud Ahmadi
// CS 890 - Topics on Communications
// Final project
// March 29, 2019
//
//

const AppDb = require('./applicationDb.js');


/**
 * Repository class that interacts with application db to store and query 
 * devices table stored on local sqlite3 database
 */
class DeviceRepository {
    constructor(){
        this._location = "U of R";
        this._latitude = 50.4154926;
        this._longitude = -104.5885128;
        
        // Set path for local db
        this._db = new AppDb(__dirname + '/db/bt_devices.db');

        // Create devices table
        this._db.run(`CREATE TABLE IF NOT EXISTS devices (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            mac VARCHAR(255),
            device VARCHAR(255),
            lastAccess DATETIME,
            location VARCHAR(50),
            latitude double,
            longitude double
        )`);
    }
    
    /**
     * Update devices table based on incoming bluetooth data     
     */
    updateDb(data){
        for(var i = 0; i < data.length; i++){
            this.insert(data[i]);
        }
    }

    /**
     * Insert bluetooth data into devices table     
     */
    insert(data){
        console.log('inserting', data);
        return this._db.run(
            `INSERT INTO devices (mac, device, lastAccess, location, latitude, longitude)
            VALUES (?, ?, ?, '${this._location}', '${this._latitude}', '${this._longitude}')`,
            data)
            .then(this.removeDuplicateMACs(data));
    }

    /**
     * Remove duplicate entries from devices table
     */
    removeDuplicateMACs(data){      
        // Delete duplicate records and keep record 
        // with OLDEST timestamp
        return this._db.run(`
        DELETE FROM devices 
        WHERE 
        rowid NOT IN (SELECT min(rowid) FROM devices GROUP BY mac);`);
        
        // // Another approach of removing duplicate records
        // // This method will keep the LATEST timestamp
        // return this._db.run(`
        // DELETE FROM devices 
        // WHERE mac = '${data[0]}' AND lastAccess <> '${data[2]}'`);
    }

    /**
     * Remove all records from table
     */
    truncateTable(){
        return this._db.run(`DELETE FROM devices;`);
    }

    /**
     * Return all data in devices table
     */
    all(sql){
        return this._db.all(sql);
    }
}

module.exports = DeviceRepository;