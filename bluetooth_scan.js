// Massoud Ahmadi
// CS 890 - Topics on Communications
// Final project
// March 29, 2019
//
//

const { spawn } = require('child_process');
const DeviceRepository = require('./deviceRepository.js');
const CloudDB = require('./cloudDb.js');
const axios = require('axios');

/**
* Bluetooth scanner class for collecting and processing bluetooth data 
* from devices
*/
class BlueToothScanner {
  constructor(){
    this._bluetoothCtl = null;
    this._cloudDb = new CloudDB();
    this._deviceRepo = new DeviceRepository();

    // Upload data to cloud every 5 minutes
    setInterval(() =>{
      this._uploadToCloud(this._deviceRepo, this._cloudDb);
    }, 300000 /*5 minutes*/);
  }

  /**
  * Start scanning for bluetooth signals, parse them and store them in local database
  */
  start(){
    if(this._bluetoothCtl == null){
      this._bluetoothCtl = spawn('bluetoothctl');

      this._bluetoothCtl.stdout.on('data', (data) => {
        var parseData = this._parseData(`${data}`);
        this._deviceRepo.updateDb(parseData);
      });
      
      this._bluetoothCtl.stdin.write('remove *\n');
      this._bluetoothCtl.stdin.write('scan on\n');
      
      console.log('bluetooth scanner has started...');

      process.on('close', (code) => {
        if (code !== 0) {
          console.log(`bluetoothctl process exited with codes ${code}`);
        }
        this._bluetoothCtl.stdin.end();
      });
    }
    else {
      console.log('bluetooth scanner already started...');
    }   
  }

  /**
  * Stop scanning scanning for bluetooth signals
  */
  stop(){
    if(this._bluetoothCtl){
      this._bluetoothCtl.stdin.write('exit \n');
      this._bluetoothCtl.stdin.end();
      this._bluetoothCtl.kill();
      this._bluetoothCtl = null;

      console.log('bluetooth scanner is stopped');
    }
    else{
      console.log('bluetooth already stopped');
    }
  }

  /**
  * Parse data collected from bluetooth scanner
  */
  _parseData(data){
    var dataArray = data.split('\n');
    var date = new Date();
    var output = [];

    for(var i = 0; i < dataArray.length; i++){
      var scan = dataArray[i].split(' ');

      // Only insert new devices detected tagged with \r\u001b[K[\u001b[0;92mNEW\u001b[0m]
      if(scan[0] && scan[0].includes('\r\u001b[K[\u001b[0;92mNEW\u001b[0m]') && scan[1] && scan[1] == 'Device'){
        var deviceName = '';
        var mac = scan[2];

        for(var j = 3; j < scan.length; j++){
          deviceName += ` ${scan[j]}`;
        }

        output.push([mac, deviceName, date.toLocaleString()]);
      }
    }

    return output;
  }

  /**
  * Upload data stored in local database to google firestore, calculate od matrix on cloud and
  * finally empty local database to free storage
  */
  _uploadToCloud(repo, cloud){

      // Collect all saved devices from local database
      repo.all(`SELECT * FROM devices`)
      .then((devices) => {

        // Upload data to google firestore
        return new Promise((resolve, reject) => {
          console.log('Uploading devices to cloud ...');
            for(var i = 0; i < devices.length; i++){
              devices[i].lastAccess = new Date(devices[i].lastAccess);
              cloud.set(devices[i]);
            }
            
            resolve('Upload complete');            
        });
      })
      .then(() => {
        // Call firebase serverless function to clean and calculate od matrix
        console.log('cleaning cloud db and calculating od matrix ...');
        return axios.get('https://us-central1-btscanner-1552766373091.cloudfunctions.net/cleanDbAndBuildMatrix?location=uofr');        
      })
      .then((response) => {
        console.log('od matrix calculation complete:', response.data);
        console.log('Truncating devices table ...');
        return repo.truncateTable();
      })
      .then(() => {
        console.log('devices table truncated');
      }).catch((err) => {
        console.log(err);
      });
  }
}

module.exports = BlueToothScanner;