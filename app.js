// Massoud Ahmadi
// CS 890 - Topics on Communications
// Final project
// March 29, 2019
//
//

const Express = require('express');
const BtScanner = require('./bluetooth_scan');
const AppDb = require('./applicationDb.js');
const DeviceRepository = require('./deviceRepository.js');
let app = new Express();
let Scanner = new BtScanner();
let db = new DeviceRepository();

/**
 * Web application etry point
 */
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

/**
 * Http request to stop bluetooth scanner
 */
app.get('/stop', function(req, res){
    Scanner.stop();
    res.sendFile(__dirname + '/index.html');
});

/**
 * Http request to start bluetooth scanner 
 */
app.get('/start', function(req, res){
    Scanner.start();
    res.sendFile(__dirname + '/index.html');
});

/**
 * Ajax request to get list of stored devices
 */
app.get('/getdevicelist', function(req, res){
    db.all(`SELECT * FROM devices`)
    .then((devices) => {
        res.send(devices);
    });
});

// Create instance of application server on port 8080
app.listen(8080);

console.log('Starting bluetooth scanner...');

// Automatically start bluetooth scanner
Scanner.start();