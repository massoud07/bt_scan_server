#Bluetooth scanner and web server
- 'npm install' to download and install list of repositorys

##Required Packages:
- npm install express (for node server)
- npm install nodemon -g (for automatically running server)
- npm install sqllite3
- npm install bluebird
- npm install --save @google-cloud/firestone
- npm install axios
- ag grid
- google maps

##Environment:
- raspbian (os)
- node.js v10.15.3
- bluetooth ctl
- ssh
- remote desktop
- samba (file server)